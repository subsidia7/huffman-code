from node import Node


class HCompressor:
    def __init__(self):
        self._char_codes = dict()
        self.string = None
        self._pqueue, self.entries_dict = None, None
        self.tree = None
        self._coded_string = None

    def get_codes_table(self):
        return self._char_codes

    def get_coded_string(self, string):
        self.string = string
        if string == '':
            return ''
        self._pqueue, self.entries_dict = self._create_tree_base()
        self._pqueue.sort(key=lambda v: v.weight)
        self.tree = self._make_binary_tree()
        self.make_char_codes(self.tree)
        self._coded_string = self._code_string()
        return self._coded_string

    def _create_tree_base(self):
        entries = dict()
        for c in self.string:
            if c in entries.keys():
                entries[c] += 1
            else:
                entries.update({ c: 1 })
        return ([Node(char=key, weight=entries[key], p_left=None, p_right=None) 
                                                    for key in entries.keys()], entries)

    def _make_binary_tree(self):
        while(len(self._pqueue) != 1):
            self._pqueue[1] = Node(char=self._pqueue[0].char+self._pqueue[1].char, 
                            weight=self._pqueue[0].weight+self._pqueue[1].weight, 
                            p_left=self._pqueue[0], 
                            p_right=self._pqueue[1])
            del self._pqueue[0]
            self._pqueue.sort(key=lambda n: n.weight)
        return self._pqueue[0]

    def make_char_codes(self, tree, code=''):
        if tree.p_left != None:
            self.make_char_codes(tree.p_left, code+'0')
        if tree.p_right != None:
            self.make_char_codes(tree.p_right, code+'1')
        if tree.p_left == tree.p_right == None:
            self._char_codes[tree.char] = code

    def _code_string(self):
        scoded = ''
        for c in self.string:
            scoded += self._char_codes[c] 
        return scoded