from controller import Controller
from hcompressor import HCompressor
from view import View


if __name__ == "__main__":
	v = View()
	hc = HCompressor()
	ctrl = Controller(v, hc)
	v.mainloop()


