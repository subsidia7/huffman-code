from collections import namedtuple


Node = namedtuple('Node', 'char weight p_left p_right')