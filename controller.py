class Controller:
    def __init__(self, view, compressor):
        self.view = view
        self.view.button.config(command=self.code_string)
        self.compressor = compressor
    
    def code_string(self):
        text = self.view.encode.get('1.0', 'end')
        text = self.compressor.get_coded_string(text)
        self.view.decode.config(state='normal')
        self.view.decode.delete('0.0', 'end')
        self.view.decode.insert('0.0', text)
        self.view.decode.config(state='disabled')

